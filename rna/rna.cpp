#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char *argv[]) {
 
  string str1 = ""; 
  string line;
  
  fstream infile;
  infile.open(argv[1]);
  while(getline(infile, line))
  {
    str1 += line;
  }
  infile.close();
  
  string str2 = "";
  
  /*Main loop*/
  for(int i=0; i<str1.length(); i++) 
  {
    if(str1[i] == 'T') 
    { 
      str2 += 'U'; 
    }
    else 
    {
      str2 += str1[i];
    }
    
  }
  
  cout << str2 << endl;
  
  return 0;
}