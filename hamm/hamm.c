/*
    Evolution as a Sequence of Mistakesclick to collapse

    A mutation is simply a mistake that occurs during the creation or copying of a nucleic acid. Because nucleic acids are vital to cellular functions, mutations tend to cause a ripple effect throughout the cell. Although mutations are mistakes, a very rare mutation may equip the cell with a beneficial attribute; in fact, evolution is powered on the cellular level by the accumulated result of beneficial mutations over many generations.

    The simplest and most common type of mutation is a point mutation, which replaces one base with another at a single nucleotide (changing the complementary base as well in the case of DNA). See Figure 1 Two DNA strands from different organisms or species are homologous if they share a recent ancestor; thus, counting the number of bases at which homologous strands differ provides us with the minimum number of point mutations that could have occurred on the evolutionary path between the two strands.

    We are interested in the minimum number of possible point mutations because of the biological principle of parsimony, which demands that evolutionary histories should be as simple as possible.

    
    Problem

  Given two strings s and t of equal length, the Hamming distance between s and t, denoted dH(s,t), is the number of corresponding symbols that differ in s and t. See Figure 2.

  Given: Two DNA strings s and t of equal length (not exceeding 1 kbp).

  Return: The Hamming distance dH(s,t).
  Sample Dataset

  GAGCCTACTAACGGGAT
  CATCGTAATGACGGCCT

  Sample Output

  7

 
 
 */

#include <stdio.h>
#include <string.h>

int main() {
 
  char* str1 = "GAGCCTACTAACGGGAT";
  char* str2 = "CATCGTAATGACGGCCT";
  short hd = 0;
  /*Main loop*/
  int i=0;
  for(i=0; i<strlen(str2); i++) {
    if(str1[i] != str2[i]) { hd++; }
  }
  
  printf("%d\n",hd);
  
  return 0;
}