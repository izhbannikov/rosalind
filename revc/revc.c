/*Reverse Complement
 
 A DNA string is formed from the alphabet containing A, C, G, and T. The complement of A is T, and the complement of C is G.

The reverse complement of a DNA string s is the string sc formed by reversing the symbols of s, then taking the complement of each symbol (e.g., the reverse complement of GTCA is TGAC).

Given: A DNA string s of length at most 1000 bp.

Return: The reverse complement of s.
 
 
 */

#include <stdio.h>
#include <string.h>
#include <iostream>

using namespace std;

int main() {
 
  string input_str = "AAAACCCGGT";
  string rc = "";
  /*Main loop*/
  /*Complement*/
  for(int i=input_str.length(); i>=0; i--) {
  
    if(input_str[i] == 'A') { rc += 'T'; continue; }
    if(input_str[i] == 'G') { rc += 'C'; continue; }
    if(input_str[i] == 'C') { rc += 'G'; continue; }
    if(input_str[i] == 'T') { rc += 'A'; continue; }
    
  }
  
  cout << rc << endl;
  
  return 0;
}
