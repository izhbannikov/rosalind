#!/usr/bin/env python

from Bio import Entrez
from Bio import SeqIO
import os

Entrez.email = "your_name@your_mail_server.com"
handle = Entrez.efetch(db="protein", id=["A5A3H2 Q9HUU8"], rettype="fasta")
records = list (SeqIO.parse(handle, "fasta"))
SeqIO.write(records[0],"toalign_1.fasta","fasta")
SeqIO.write(records[1],"toalign_2.fasta","fasta")

os.system("water -asequence toalign_1.fasta -bsequence toalign_2.fasta -gapopen 10 -gapextend 1 -datafile EBLOSUM62 -outfile std.out")	
