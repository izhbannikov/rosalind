 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

//Basic scoring constants:
int indel = -5;
int mism = -5;
int match = +5;


int OPEN_GAP_PENALTY = -5;
int gap_extension_penalty = 0;

int PAM250(char c1, char c2) {

    int i,j;
    /*Ищем строчку*/
    switch(c1) {
        case 'A' : i=0; break;
        case 'R' : i=1; break;
        case 'N' : i=2; break;
        case 'D' : i=3; break;
        case 'C' : i=4; break;
        case 'Q' : i=5; break;
        case 'E' : i=6; break;
        case 'G' : i=7; break;
        case 'H' : i=8; break;
        case 'I' : i=9; break;
        case 'L' : i=10; break;
        case 'K' : i=11; break;
        case 'M' : i=12; break;
        case 'F' : i=13; break;
        case 'P' : i=14; break;
        case 'S' : i=15; break;
        case 'T' : i=16; break;
        case 'W' : i=17; break;
        case 'Y' : i=18; break;
        case 'V' : i=19; break;
        //default : return;
    }

    /*Ищем столбец*/
    switch(c2) {
        case 'A' : j=0; break;
        case 'R' : j=1; break;
        case 'N' : j=2; break;
        case 'D' : j=3; break;
        case 'C' : j=4; break;
        case 'Q' : j=5; break;
        case 'E' : j=6; break;
        case 'G' : j=7; break;
        case 'H' : j=8; break;
        case 'I' : j=9; break;
        case 'L' : j=10; break;
        case 'K' : j=11; break;
        case 'M' : j=12; break;
        case 'F' : j=13; break;
        case 'P' : j=14; break;
        case 'S' : j=15; break;
        case 'T' : j=16; break;
        case 'W' : j=17; break;
        case 'Y' : j=18; break;
        case 'V' : j=19; break;
        //default : return;
    }

    int pam250[20][20] = { {2, -2,  0,  0, -2,  0,  0,  1, -1, -1, -2, -1, -1, -3,  1,  1,  1, -6, -3,  0},
                {-2,  6,  0, -1, -4,  1, -1, -3,  2, -2, -3,  3,  0, -4,  0,  0, -1,  2, -4, -2},
                {0,  0,  2,  2, -4,  1,  1,  0,  2, -2, -3,  1, -2, -3,  0,  1,  0, -4, -2, -2},
                {0, -1,  2,  4, -5,  2,  3,  1,  1, -2, -4,  0, -3, -6, -1,  0,  0, -7, -4, -2},
                {-2, -4, -4, -5, 12, -5, -5, -3, -3, -2, -6, -5, -5, -4, -3,  0, -2, -8,  0, -2},
                {0,  1,  1,  2, -5,  4,  2, -1,  3, -2, -2,  1, -1, -5,  0, -1, -1, -5, -4, -2},
                {0, -1,  1,  3, -5,  2,  4,  0,  1, -2, -3,  0, -2, -5, -1,  0,  0, -7, -4, -2},
                {1, -3,  0,  1, -3, -1,  0,  5, -2, -3, -4, -2, -3, -5,  0,  1,  0, -7, -5, -1},
                {-1,  2,  2,  1, -3,  3,  1, -2,  6, -2, -2,  0, -2, -2,  0, -1, -1, -3,  0, -2},
                {-1, -2, -2, -2, -2, -2, -2, -3, -2,  5,  2, -2,  2,  1, -2, -1,  0, -5, -1,  4},
                {-2, -3, -3, -4, -6, -2, -3, -4, -2,  2,  6, -3,  4,  2, -3, -3, -2, -2, -1,  2},
                {-1,  3,  1,  0, -5,  1,  0, -2,  0, -2, -3,  5,  0, -5, -1,  0,  0, -3, -4, -2},
                {-1,  0, -2, -3, -5, -1, -2, -3, -2,  2,  4,  0,  6,  0, -2, -2, -1, -4, -2,  2},
                {-3, -4, -3, -6, -4, -5, -5, -5, -2,  1,  2, -5,  0,  9, -5, -3, -3,  0,  7, -1},
                {1,  0,  0, -1, -3,  0, -1,  0,  0, -2, -3, -1, -2, -5,  6,  1,  0, -6, -5, -1},
                {1,  0,  1,  0,  0, -1,  0,  1, -1, -1, -3,  0, -2, -3,  1,  2,  1, -2, -3, -1},
                {1, -1,  0,  0, -2, -1,  0,  0, -1,  0, -2,  0, -1, -3,  0,  1,  3, -5, -3,  0},
                {-6,  2, -4, -7, -8, -5, -7, -7, -3, -5, -2, -3, -4,  0, -6, -2, -5, 17,  0, -6},
                {-3, -4, -2, -4,  0, -4, -4, -5,  0, -1, -1, -4, -2,  7, -5, -3, -3,  0, 10, -2},
                {0, -2, -2, -2, -2, -2, -2, -1, -2,  4,  2, -2,  2, -1, -1, -1,  0, -6, -2,  4}};

    return pam250[i][j];
}

int tmp_i, tmp_j;
/*Скорринговая функция*/
int score_func(char x, char y) {
    
    if( (x == '-') || (y == '-') ) {
      return OPEN_GAP_PENALTY;
    } else {
      return PAM250(x,y);
    }
    
   

}

/*Скорринговая функция*/
int score_func2(char x, char y) {
    
    if( (x == '-') || (y == '-') ) {
      return indel;
    } else { 
    
    //if(x != y)
      return PAM250(x,y);//mism;
    }
    
     //return match;

}

long max3(int x, int y, int z) {
    int max = x;

    if( y > max )
        max = y;

    if( z > max )
        max = z;

    return max;
}

string s1 = "MEANLYPRTEINSTRING";
string s2 = "PLEASANTLYEINSTEIN";

long **S = NULL;

int main(int argc, char argv[]) {
  

    /*Инициализация скорринговой матрицы*/
    S = new long*[s1.length() + 1];
    for (int k = 0; k < s1.length()+1; k++)
        S[k] = new long[s2.length() + 1];

    int i, j;
    for(i=0; i<s1.length() + 1; i++ ) {
        for(j=0; j<s2.length() + 1; j++ ) {
            S[i][j] = 0;
        }
    }

    for(i=0; i<s1.length() + 1; i++ ) {
        S[i][0] = i*OPEN_GAP_PENALTY;
    }

    for(j=0; j<s2.length() + 1; j++ ) {
        S[0][j] = j*OPEN_GAP_PENALTY;
    }

    for( i=1; i<s1.length() + 1; i++) {
        char s1_i = s1[i-1];
            for(j=1; j<s2.length() +1; j++ ) {
            char s2_j = s2[j-1];
            S[i][j] = (max3( S[i-1][j] + score_func2(s1_i, '-'), S[i][j-1] + score_func2('-', s2_j), S[i-1][j-1] + score_func2(s1_i, s2_j) ) < 0 ) ? 0 : max3( S[i-1][j] + score_func2(s1_i, '-'), S[i][j-1] + score_func2('-', s2_j), S[i-1][j-1] + score_func2(s1_i, s2_j)) ;
        }
    }

    string al_s1 = "";
    string al_s2 = "";
    i = s1.length();
    j = s2.length();
    char s1_i, s2_i;
    while( (i>0) || (j>0) ) {
      
      if (i>0) s1_i = s1[i-1];
      if (j>0) s2_i = s2[j-1];
    
      if ( (i==0) || (S[i][j]==S[i][j-1] + indel) ) {
        al_s1 = '-' + al_s1;
        al_s2 = s2_i + al_s2;
        j = j-1;
      }
      else if ( (j==0) || (S[i][j]==S[i-1][j] + indel) ) {
        al_s1 = s1_i + al_s1;
        al_s2 = '-' + al_s2;
        i = i-1;
      }
     else {
        al_s1 = s1_i + al_s1;
        al_s2 = s2_i + al_s2;
        i = i-1;
        j = j-1;
     }
    }
    //Наилучшие очки для глобального выравнивания
    //cout << S[i-1][j-1] << endl;
    i = s1.length();
    j = s2.length();
    cout << S[i-1][j-1] << endl;
    cout << al_s1 << endl;
    cout << al_s2 << endl;

    return 0;

}