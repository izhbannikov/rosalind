#include <stdio.h>
#include <string>
#include <map>
#include <iostream> 
#include <fstream> 
#include <vector>

using namespace std;

int main( int argc, char *argv[] ) {
 	vector<string> dnas;
	int cnta, cntc, cntg, cntt; 
	/*Loading results from file*/
	std::ifstream infile;
	infile.open(argv[1]); 
	string line;
	while( getline(infile, line) ) {
  		dnas.push_back(line);
	}
	
	infile.close();
	/*Main loop*/
  	/*Making a profile array*/
	int **parr = NULL;
	int size = dnas[0].length();
	
	parr = new int*[size];
	
	for(int i=0; i<size; i++) {
		parr[i] = new int[size];
	}
	
	cnta = cntc = cntg = cntt = 0;
	for(int j=0; j<size; ++j) {
		cnta = cntc = cntg = cntt = 0;
		for(int i=0; i<dnas.size(); i++) {
			if(dnas[i][j] == 'A') { cnta++; }
			if(dnas[i][j] == 'C') { cntc++; }
			if(dnas[i][j] == 'G') { cntg++; }
			if(dnas[i][j] == 'T') { cntt++; }
			
			
		}
		
		parr[0][j] = cnta;
		parr[1][j] = cntc;
		parr[2][j] = cntg;
		parr[3][j] = cntt;
		
		
	}

	/*Makig a consensus string*/
	string cons_str, tmp;
  	for(int j=0; j<size; j++) {
		short max = parr[0][j];
		tmp = "A";
		for(int i=0; i<dnas.size(); i++) {
			if(max <  parr[i][j]) {
				max =  parr[i][j];
				switch(i) {
					case 0 : tmp = "A"; break;
					case 1 : tmp = "C"; break;
					case 2 : tmp = "G"; break;	
					case 3 : tmp = "T"; break;
				}
			}
	
						
		}
		
		cons_str += tmp;
		

	}
  	
	/*printing results*/
	cout << cons_str << endl;
	
	for(int i=0; i < 4; i++) {
		
		switch(i) {
			case 0 : cout << "A: "; break;
			case 1 : cout << "C: "; break;
			case 2 : cout << "G: "; break;
			case 3 : cout << "T: "; break;
		}

		for( int j=0; j<dnas[0].length(); j++ ) {
			cout << parr[i][j] << " ";
		}
  
  		cout << "\n";
	}

  	return 0;
}
