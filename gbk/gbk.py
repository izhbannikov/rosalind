#!/usr/bin/env python

from Bio import Entrez

Entrez.email = "your_name@your_mail_server.com"
handle = Entrez.esearch(db="nucleotide", term='Aminomonas[Organism] AND ("2006/11/16"[PDAT] : "2012/02/08"[PDAT])')
record = Entrez.read(handle)
print record["Count"]
