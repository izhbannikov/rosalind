#!/usr/bin/env python

taxa = []
pct = []

#This function gives us all possible combinations whithin A and B
def get_pair( array ) :
  output = list()
  m=0
  while m < len(array) :
    n=m+1
    while n < len(array) :
      t = list()
      t.append(taxa[array[m]])
      t.append(taxa[array[n]])
      output.append(t)
      n+=1
    m+=1

  return output
	  
file = open('sample2.txt')
#file = open('rosalind_qrt.txt')

#Reading input file
k = 0
while 1:
  line = file.readline()
  
  if not line:
    break
  
  if k == 0 : 
    for item  in line.strip().split(' ') :
      taxa.append(item.strip())
  else :
    pct.append(line.strip())
  
  k+=1
  

tmp_final = list()
final = list()
for item in pct :
  #A and B represent two subsets of a quartet.
  #A contains ones only and B contains zeros only
  A = []
  B = []
  for i in range(len(item)) :
    if item[i] == '1' :
      A.append(i)
    elif item[i] == '0' :
      B.append(i)
  #Make sure that those are not trivial characters
  if (len(A) > 1) and (len(B) > 1) :
    #Make all possible combinations whithin A and B
    AA = get_pair(A)
    BB = get_pair(B)
    for ii in range(len(AA)) :
      for jj in range(len(BB)) :
	t = list()
	t.append(AA[ii])
	t.append(BB[jj])
	tmp_final.append(t)
	
#Main loop. Here we have to check that there are no any doubles presented.    
i = 0
while i < len(tmp_final) :
  flag = True
  j=i+1
  while j < len(tmp_final) :
    if ( ( len(set(tmp_final[i][0]).difference(tmp_final[j][0])) == 0 ) and ( len(set(tmp_final[i][1]).difference(tmp_final[j][1])) == 0 ) ) or ( ( len(set(tmp_final[i][0]).difference(tmp_final[j][1])) == 0 ) and ( len(set(tmp_final[i][1]).difference(tmp_final[j][0])) == 0 ) ) :
      flag = False
    j+=1
  if flag == True :    
    final.append(tmp_final[i])
  i+=1


#Output results
f1=open('out.txt', 'w+')
for item in final :
  f1.write('{'+item[0][0] + ', ' + item[0][1] + '}' + ' ' + '{' + item[1][0] + ', ' + item[1][1] + '}' + '\n')
  print '{'+item[0][0] + ', ' + item[0][1] + '}' + ' ' + '{' + item[1][0] + ', ' + item[1][1] + '}'
    
