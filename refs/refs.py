#!/usr/bin/env python

from Bio import Entrez

Entrez.email = "your_name@your_mail_server.com"
handle = Entrez.esearch(db="nucleotide", term='\"Propionibacterium\"[Organism] AND srcdb_refseq[PROP] AND (\"1986/1/1\"[PDAT] : \"2011/05/01\"[PDAT]) AND (911:988[SLEN])',retmax=50000)
record = Entrez.read(handle)
print record["Count"]
