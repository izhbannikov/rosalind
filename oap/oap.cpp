#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector.h>

using namespace std;

//Two sequences you want to align
string seq11 = "CTAAGGGATTCCGGTAATTAGACAG";
string seq22 = "ATAGACCATATGTCAGTGACTGTGTAA";

int match_award = 1;
int mismatch_penalty = -2;
int gap_penalty = -2;
      
//Creates empty matrix with all zeros
int **zeros(int nrows, int ncols) {
    int **retval = new int*[nrows];
    for(unsigned int i=0; i < nrows; ++i) {
        retval[i] = new int[ncols];
        for(unsigned int j = 0; j < ncols; ++j) {
            retval[i][j] = 0;
	}
    }
    
    return retval;
}

//No substituition matrix, just simple linear gap penalty model
int match_score(char alpha, char beta) {
    if (alpha == beta) {
        return match_award;
    }
    else if ( (alpha == '-') || (beta == '-') ) {
        return gap_penalty;
    }
    
    return mismatch_penalty;
}

void oap(string seq1, string seq2) {
    
    unsigned int m = seq1.length();
    unsigned int n = seq2.length(); //lengths of two sequences
    unsigned int i,j;
    i = j = 0;
    //Generate DP (Dynamic Programmed) table and traceback path pointer matrix
    int **score = zeros(n+1, m+1); //The DP table
    for(i = 0; i < n+1; i++) {
      score[i][0] = 0;
    }
    
    for(j =0; j < m+1; j++) {
      score[0][j] = 0;
    }
    
    //Traceback matrix
    int **pointer = zeros(n+1, m+1); //to store the traceback path
    for(i = 0; i <  n+1; i++) {
      pointer[i][0] = 1;
    }
    for(j = 0; j < m+1; j++) {
      pointer[0][j] = 2;
    }
    
    //Calculate DP table and mark pointers
    for(i = 1; i < n + 1; ++i) {
        for(j = 1; j < m + 1; ++j) {
            int score_diagonal = score[i-1][j-1] + match_score(seq1[j-1], seq2[i-1]);
            int score_up = score[i][j-1] + gap_penalty;
	    int score_left = score[i-1][j] + gap_penalty;
	    score[i][j] = max(score_left, max(score_up, score_diagonal));
	    
	    if(score[i][j] == score_diagonal ) {
                pointer[i][j] = 3; //3 means trace diagonal
	    }
	    else if(score[i][j] == score_up) {
                pointer[i][j] = 2; //2 means trace left
	    }
	    else if(score[i][j] == score_left) {
                pointer[i][j] = 1; //1 means trace up
	    }
	}
    }        
    int max_i = -200;
    for(unsigned int ii = 0; ii < n+1; ++ii) {
      if(score[ii][m] >= max_i) {
	max_i = score[ii][m];
	i = ii;
      }
    }
    
    cout << max_i << endl;
    
    j--;
    i--;
    
    //Traceback, follow pointers in the traceback matrix
    string align1, align2; // initial sequences
    align1 = align2 = "";
    while(1) { 
      if(pointer[i][j] == 3) {
	align1 = seq1[j-1] + align1;
	align2 = seq2[i-1] + align2;
	i -= 1;
	j -= 1;
      }
      else if(pointer[i][j] == 2) { //2 means trace left
	align2 = '-' + align2;
	align1 = seq1[j-1] + align1;
	j -= 1;
      }
      else if(pointer[i][j] == 1) { //1 means trace up
	align2 = seq2[i-1] + align2;
	align1 = '-' + align1;
	i -= 1;
      }
      if ( (i == 0) || (j == 0) )
	break;
    }
    
    cout << align1 << '\n' << align2 << '\n';
     
}

int main(int argc, char **argv) {
  char* in_filename = "in.fastq";
  char* out_filename = "report.csv";
  int min_score = 1;
  
  string<vector> reads;
  string<vector> ids;
  
  fstream in;
  string line;
  in.open(in_filename);
  short ii = 0;
  while(getline(in,line)) {
    if(ii == 0)
    {
      ids.push_back(line);
    }
    if(ii == 1)
    {
      reads.push_back(line); 
    }
    ii++;
    if(ii = 3) ii = 0;
  }
  
  for(unsigned int i = 0; i < reads.size(); ++i) {
    for(unsigned int j = i+1; j < reads.size(); ++j) {
      oap(seq11,seq22);
    }
    
  }
  
}

   

