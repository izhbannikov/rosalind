#include <stdio.h>
#include <iostream>
#include <boost/regex.hpp> // add boost regex library
//#include <regex>
#include <map>

using namespace std;


void GetMotifs(string str,string name)
{
  map<int, string > res;
  
  boost::regex xRegEx1("[N][^P][S][^P]");
  boost::smatch xResults1;
  boost::sregex_iterator search1(str.begin(), str.end(), xRegEx1);
  boost::sregex_iterator xInvalidIt1;
  
  while(search1 != xInvalidIt1)
  {
    int pos = (*search1++).position()+1;
    res.insert(std::pair<int, string >(pos, ""));
  }
  
  boost::regex xRegEx2("[N][^P][T][^P]");
  boost::smatch xResults2;
  boost::sregex_iterator search2(str.begin(), str.end(), xRegEx2);
  boost::sregex_iterator xInvalidIt2;
  while(search2 != xInvalidIt2)
  {
    int pos = (*search2++).position()+1;
    res.insert(std::pair<int, string >(pos, ""));
  }
  
  //cout << endl;
  if (res.size() > 0)
  {
    cout << name << endl;
    map<int, string> ::iterator it;
    for (it = res.begin(); it != res.end(); ++it) 
    {
      cout << (*it).first << " ";
    }
    cout << endl;
  }
  
}
