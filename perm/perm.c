#include <stdio.h>
#include <string.h>
#include <iostream> /*нужен для cout*/
#include <map>
#include <fstream> /*нужен для ifstream*/
#include <list>
#include <streambuf>
#include <vector>
#include <deque>
#include <stdlib.h>     /* atoi */

using namespace std;

class Node
{
	public:
  		string name;
  		int value;
  		vector<Node*> linkedNodes;
		Node *parent;
};

vector<Node*> graph;

short k;
unsigned int num_perm = 0;

void addLinkedNodes(Node *g_node, deque<int> nums) {
	/*Making a new node*/
	for(int a=0; a<nums.size(); a++) {
		Node *new_node = new Node();
		new_node->value = nums[a];
		new_node->parent = g_node;			
		graph.push_back(new_node);
		
		deque<int> ncopy;
		for(int k=0; k<nums.size(); k++) {
			ncopy.push_back(nums[k]);
		}
		ncopy.erase(ncopy.begin()+a);
		g_node->linkedNodes.push_back(new_node);
		addLinkedNodes(new_node, ncopy);
	}
	return;
	
}

void MakeGraph() {
  /*First we have to create a root*/
  deque<int> nums;
  for(int i=1; i<=k; i++) {
    nums.push_back(i);
  }
  Node *root_node = new Node();
  graph.push_back( root_node );
  root_node->value = 0;//nums[0];
  addLinkedNodes( root_node, nums );
}

void print(Node *g_node) {
	if(g_node->value != 0) {
		if(g_node->parent->value == 0) {
			cout << g_node->value << endl;
			num_perm += 1;
		} else {
			cout << g_node->value << " ";
		}
	} 

	if(g_node->parent != NULL)
		print(g_node->parent);
}

int main( int argc, char *argv[] ) {
	k = atoi( argv[1] ); 	
	
	MakeGraph();

	int i;
	/*Printing results*/
	for(i=0; i<graph.size(); i++) {
		if(graph[i]->linkedNodes.size() == 0) { /*It can be optimized if add such nodes into separate layer*/	
			print(graph[i]);
		}
	
	}
	cout << "Total number of permutations: " << num_perm << endl;

}
