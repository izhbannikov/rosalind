#!/usr/bin/env python
from Bio import SeqIO
from Bio.Seq import Seq

cnt = 0;

records = list(SeqIO.parse("rosalind_rvco.txt", "fasta"))

for r in records :
  rev = r.reverse_complement()
  
  if str(rev.seq) == str(r.seq) :
    cnt += 1
    
print cnt
