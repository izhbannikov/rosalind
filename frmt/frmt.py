#!/usr/bin/env python

from Bio import Entrez
from Bio import SeqIO

Entrez.email = "your_name@your_mail_server.com"
handle = Entrez.efetch(db="nucleotide", id=["JX308815 NM_001185098 BT149870 JX462669 NM_001197168 NM_001135551 JX462670 NM_001246828 JQ011276 JX317624"], rettype="fasta")
records = list (SeqIO.parse(handle, "fasta"))
rs = sorted(records, key=lambda rec: len(rec.seq))[0]
print '>' + rs.description
print rs.seq
	
