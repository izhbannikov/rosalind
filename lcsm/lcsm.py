#!/usr/bin/python

seqs = [] 
f = open('rosalind_lcsm.txt') #sample.fasta
tmpstr = ""
for line in f :
   if line.strip()[0] != '>' :     
      tmpstr += line.strip()
   else :
      if tmpstr != "" :
	seqs.append(tmpstr)
	tmpstr = ""
#For last record in a file:    
seqs.append(tmpstr)
     

sort_key = lambda s: (-len(s), s)
seqs.sort(key=sort_key)

subSeqs = []

for subLen in range(1,len(seqs[0])):
    foundSub = False
    for i, char in enumerate(seqs[0]):
        if subLen + i < len(seqs[0]):
            subSeq = seqs[0][i:i + subLen]
            if all(subSeq in seq for seq in seqs[1:]):
                subSeqs.append(subSeq)
                foundSub = True
    if not foundSub:
        break

print max(subSeqs, key=len)
  
