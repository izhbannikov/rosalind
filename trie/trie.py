#!/usr/bin/env python

from collections import defaultdict
last_label = 1 
class Trie:
    def __init__(self):
	global last_label        
	self.root = defaultdict(Trie)
        self.value = None
	self.fr = 0
	self.label = 1
	
 
    def add(self, s, value):
        """Add the string `s` to the 
        `Trie` and map it to the given value."""
	global last_label	
	head = s[0]
	tail = s[1:]
        cur_node = self.root[head]
	
	if cur_node.label == 1 :
		last_label += 1
		cur_node.fr = self.label	
		cur_node.label = last_label	
	if not tail:
	    cur_node.value = value
	    return  # No further recursion
	
	cur_node.add(tail, value)
 
    def items(self) :
	global f
	"Return an iterator over the items of the `Trie`."
	for char, node in self.root.iteritems():
		print node.fr, node.label, char
		f.write(str(node.fr) + " " + str(node.label) + " " + char + '\n')
		if node.value is None:
			node.items()



r = Trie()
f = open('out.txt','w')
#r.add("ATAGA",1)
#r.add("ATC",2)
#r.add("GAT",3)
strings = open("rosalind_trie.txt", "r").readlines()
for s in strings :
	r.add(s[:-1],0)
	
r.items()




