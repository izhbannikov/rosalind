#!/usr/bin/env python

"""
By Matthew Tadd http://rosalind.info/users/mtadd/
"""

import re

def parseNewickTree(s):
    
    tree = [] 
    for name, op in re.findall("(?:(\w+)|(.))",s):
        if name:
            tree.append( name ) 
        elif op == ')':
            right = tree.pop()
            left = tree.pop()
            tree.append( [left, right] )
    return tree

def readNewickTree(s):
    
    m = re.match("(\w+)\((.+)\);",s) # z(y);
    print m
    if m:
        return parseNewickTree(m.group(2)), m.group(1)
    m = re.match("\((.+)\)(\w+);",s) # (y)z;
    
    if m:
        return parseNewickTree(m.group(1)), m.group(2)
    
    raise ValueError

def visit(node, dobranch=None, doleaf=None):
    def walk(node):
        if type(node) in [list, tuple]:
            for c in node:
                walk(c)
            if dobranch:
                dobranch(node)
        elif type(node) == str:
            if doleaf:
                doleaf(node)
    walk(node)

def charArray(node):
    children = set()
    for c in node:
        visit(c, doleaf=lambda n: children.add(n))
    if len(children) <= 1 or len(allNodes-children) <= 1:
        return  # trivial character
    print ''.join(['1' if n in children else '0' for n in sortedNodes])

if __name__ == "__main__":
    import sys
    t = "(rat,(dog,cat),(rabbit,(elephant,mouse)));"
    tree = readNewickTree(sys.stdin.readline().strip())
    allNodes = set()
    visit(tree, doleaf=lambda n: allNodes.add(n))
    sortedNodes = sorted(allNodes)
    visit(tree, dobranch=charArray) 
