#!/usr/bin/env python
from StringIO import StringIO
from Bio import Phylo
import sys
import itertools
import numpy
import networkx, pylab

chars = dict() 
char_matrix = []

t = Phylo.read(StringIO('(rat,(dog,cat),(rabbit,(elephant,mouse)));'), 'newick')

#List of terminals
for c in list(t.get_terminals()) :
    chars[c.name] = []

#Making an adjacency matrix. Thanks to networkx package
net = Phylo.to_networkx(t)
amatrix = networkx.adjacency_matrix(net)
tchars = []
for node in net.nodes(data=True) :
  tchars.append(str(node[0]))

#Actual algorithm
for m in range(len(amatrix)) : 
  if amatrix[m,:].sum() == 3 :
    for i in range(m) :
      if (i != m) and (amatrix[i,:].sum() == 3) and (amatrix[i,m] == amatrix[m,i]) and (amatrix[i,m] == 1) :
	  amatrix[i,m] = 0
	  amatrix[m,i] = 0
	  net=networkx.from_numpy_matrix(amatrix)
	  test1 = networkx.connected_components(net)
	  for item in test1[0] :
	    try :
	      chars[tchars[int(item)]].append(1)
	    except :
	      cc =2
	  for item in test1[1] :
	    try :
	      chars[tchars[int(item)]].append(0)
	    except :
	      cc =2  
		
	  amatrix[i,m] = 1
	  amatrix[m,i] = 1


#Creating the final character matrix
for i in xrange(len(chars.items()[0][1])):
    char_matrix.append([])
    for j in xrange(len(chars)):
      char_matrix[i].append(0)



nn = 0
for k, v in sorted(chars.items()) :
  for j in range(len(v)):
    char_matrix[j][nn] = v[j]
  nn+=1

for i in xrange(len(char_matrix)):
  str1 = ""
  for j in xrange(len(char_matrix[i])):
      str1 += str(int(char_matrix[i][j]))
  print str1 
  
    
