#!/usr/bin/env python

#Two sequences you want to align
#from Bio import SeqIO

#records = list(SeqIO.parse("rosalind_smgb.txt", "fasta"))

#Two sequences you want to align
#seq11 = records[0].seq
#seq22 = records[1].seq

seq11 = "CAGCACTTGGATTCTCGG" 
seq22 = "CAGCGTGG"


match_award = 1
mismatch_penalty = -1
gap_penalty = -1 


#Creates empty matrix with all zeros
def zeros(shape):
    retval = []
    for x in range(shape[0]):
        retval.append([])
        for y in range(shape[1]):
            retval[-1].append(0)
    return retval

#No substituition matrix, just simple linear gap penalty model
def match_score(alpha, beta):
    if alpha == beta:
        return match_award
    elif alpha == '-' or beta == '-':
        return gap_penalty
    else:
        return mismatch_penalty


def nw(seq1, seq2):
    m = len(seq1)
    n = len(seq2) # lengths of two sequences
    
    # Generate DP (Dynamic Programmed) table and traceback path pointer matrix
    score = zeros((n+1, m+1)) # the DP table
    for i in range(n+1) :
      score[i][0] = i*0
    
    for j in range(m+1) :
      score[0][j] = j*0
    
    #Traceback matrix
    pointer = zeros((n+1, m+1)) # to store the traceback path
    for i in range(n+1) :
      pointer[i][0] = 1
    for j in range(m+1) :
      pointer[0][j] = 2
    
    # Calculate DP table and mark pointers
    for i in range(1, n + 1):
        for j in range(1, m + 1):
            score_diagonal = score[i-1][j-1] + match_score(seq1[j-1], seq2[i-1])
            score_up = score[i][j-1] + gap_penalty
	    score_left = score[i-1][j] + gap_penalty
	    score[i][j] = max(score_left, score_up, score_diagonal)
	    
	    if score[i][j] == score_diagonal :
                pointer[i][j] = 3 # 3 means trace diagonal
	    if score[i][j] == score_up :
                pointer[i][j] = 2 # 2 means trace left
	    if score[i][j] == score_left :
                pointer[i][j] = 1 # 1 means trace up
	    
            
    max_i = i
    max_j = j

    

    align1, align2 = '', '' # initial sequences
    
    ## Print traceback matrix as a table:
    #print "    *",
    #for j in range(len(seq1)):
    #  print " ", seq1[j],
    #print
    
    #for i in range(len(seq2)+1):
    #  print ("*" if i==0 else seq2[i-1]),
    #  for j in range(len(seq1)+1):
    #    print "%3d" % pointer[i][j],
    #  print
    
    #Print scoring matrix
    #print "    *",
    #for j in range(len(seq1)):
    #  print " ", seq1[j],
    #print
    
    #for i in range(len(seq2)+1):
    #  print ("*" if i==0 else seq2[i-1]),
    #  for j in range(len(seq1)+1):
    #    print "%3d" % score[i][j],
    #  print
    
    
    #i = max_i
    #j = max_j # indices of path starting point
    
    max_j = score[-1].index(max(score[-1]))
    print max(score[-1])
    
    #Traceback, follow pointers in the traceback matrix
    while 1 :
	if j > max_j :
	    align1 = seq1[j-1] + align1
	    align2 = '-' + align2
	    j -= 1
	    continue
	if pointer[i][j] == 3 :
            align1 = seq1[j-1] + align1
            align2 = seq2[i-1] + align2
            i -= 1
            j -= 1
        elif pointer[i][j] == 2 : #2 means trace left
            align2 = '-' + align2
	    align1 = seq1[j-1] + align1
            j -= 1
        elif  pointer[i][j] == 1 : # 1 means trace up
            align2 = seq2[i-1] + align2
            align1 = '-' + align1
	    i -= 1
        if (i == 0 and j == 0) : break
	
    
    print align1 + '\n' + align2
    
    
nw(seq11, seq22)
