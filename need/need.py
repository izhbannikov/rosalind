#!/usr/bin/env python

from Bio import Entrez
from Bio import SeqIO
import os

Entrez.email = "your_name@your_mail_server.com"
handle = Entrez.efetch(db="nucleotide", id=["JQ011276.1 JF927163.1"], rettype="fasta")
records = list (SeqIO.parse(handle, "fasta"))
SeqIO.write(records[0],"toalign_1.fasta","fasta")
SeqIO.write(records[1],"toalign_2.fasta","fasta")

os.system("needle -asequence toalign_1.fasta -bsequence toalign_2.fasta -gapopen 11 -gapextend 1 -endweight Y -endopen 11  -endextend 1 -outfile std.out")	
