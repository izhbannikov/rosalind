import math

k = 6
n = 16

def Ber(gen,num_offspring):
  return (math.factorial(2**gen)/(math.factorial(num_offspring) * math.factorial((2**gen) - num_offspring)))*(0.25**num_offspring) * (1-0.25)**((2**gen) - num_offspring)

prob = 0.0
i = n
while i <= (2**k):
  prob += Ber(k,i)
  i += 1

print prob
