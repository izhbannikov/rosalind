#!/usr/bin/env python

file = open('rosalind_ini5.txt')

k = 1
while 1:
  line = file.readline()
  
  if not line:
    break
  
  if k%2 == 0 : 
    print line[:-1]
  
  k+=1

