/*
    
    A Brief Introduction to Graph Theoryclick to collapse

    Networks arise everywhere in the practical world, especially in biology. Networks are prevalent in popular applications such as modeling the spread of disease, but the extent of network applications spreads far beyond popular science. Our first question asks how to computationally model a network without actually needing to render a picture of the network.

    First, some terminology: graph is the technical term for a network; a graph is made up of hubs called nodes (or vertices), pairs of which are connected via segments/curves called edges. If an edge connects nodes v and w, then it is written {v,w} (or equivalently {w,v}).

        an edge {v,w} is incident to nodes v and w; we say that v and w are adjacent to each other;
        the degree of v is the number of edges incident to it;
        a walk is an ordered collection of edges for which the ending node of one edge is the starting node of the next (e.g., {v1,v2}, {v2,v3}, {v3,v4}, etc.);
        a path is a walk in which every node appears in at most two edges;
        path length is the number of edges in the path;
        a cycle is a path whose final node is equal to its first node (so that every node is incident to exactly two edges in the cycle); and
        the distance between two vertices is the length of the shortest path connecting them.

    Graph theory is the abstract mathematical study of graphs and their properties.


	Problem

A graph whose nodes have all been labeled can be represented by an adjacency list, in which each row of the list contains the two node labels corresponding to a unique edge.

A directed graph (or digraph) is a graph containing directed edges, each of which has an orientation. That is, a directed edge is represented by an arrow instead of simply a segment; the starting and ending nodes of an edge form its tail and head, respectively. The directed edge with tail v and head w is represented by (v,w) (but not by (w,v)). A directed loop is a directed edge of the form (v,v).

For a collection of strings and a positive integer k, their overlap graph is a directed graph in which each string is represented by a node, and string s is connected to string t with a directed edge iff there is a length k suffix of s that matches some prefix of t. Directed loops are not allowed in the overlap graph.

Given: A collection of DNA strings in FASTA format having total length at most 10 kbp.

Return: The adjacency list corresponding to the overlap graph for k=3.
Sample Dataset

>Rosalind_1
AAATAAA
>Rosalind_2
AAATTTT
>Rosalind_3
TTTTCCC
>Rosalind_4
AAATCCC
>Rosalind_5
GGGTGGG

Sample Output

Rosalind_1 Rosalind_2
Rosalind_1 Rosalind_4
Rosalind_2 Rosalind_3

 */

#include <stdio.h>
#include <string>
#include <map>
#include <iostream> 
#include <fstream> 
#include <vector>
#include <streambuf>
using namespace std;

typedef struct {
	string seqid;
	string value;
	vector<string> adj_list;
	string prefix;
	string suffix;
} Node;

vector<Node> nodes;

short k = 3;

int main( int argc, char *argv[] ) {
 	/*Loading results from file*/
	std:fstream infile;
	infile.open(argv[1]); 
	std::string line;
	string seqid;
	
	int line_cnt = 0;
	string seqid_max;
	
	string seq;
	
	while( getline(infile, line) ) {
		
  		if(line.substr(0,1) == ">") {
			if(line_cnt != 0) {
				Node new_node;
				new_node.seqid = seqid;
				new_node.value = seq;
				new_node.prefix = seq.substr( 0, k );
				new_node.suffix = seq.substr( seq.length() - k, k );
				nodes.push_back(new_node);
				seq.clear();
			}
			seqid = line.substr(1,line.length() - 1);
			continue;		
		}
		
		seq+=line;
		line_cnt++;
		
	}
	infile.close();
	/*Processing the last line*/
	/*parsing give genomic string*/
	Node new_node;
	new_node.seqid = seqid;
	new_node.value = seq;
	new_node.prefix = seq.substr( 0, k );
	new_node.suffix = seq.substr( seq.length() - k, k );
	nodes.push_back(new_node);
	
	/*Building an adjacency list for each node*/
	for( int i=0; i<nodes.size(); i++ ) 
	{
	  for( int j=0; j<nodes.size(); j++ ) 
	  {
	     if( nodes[i].seqid != nodes[j].seqid ) 
	     {
		if(nodes[i].suffix == nodes[j].prefix) 
		{
		   bool flag = false;
		   /*In order to avoid loops:*/
		   for(int s=0; s<nodes[i].adj_list.size(); s++) 
		   {
			if( nodes[i].adj_list[s] == nodes[j].seqid ) 
			{
			   flag = true; break;						
			}
		   }
		   if(flag == false)					
			nodes[i].adj_list.push_back( nodes[j].seqid );
		}
	     }
	  }
	}

	/*making output*/
	for(int i=0; i<nodes.size(); i++) 
	{
		for( int j=0; j<nodes[i].adj_list.size(); j++ ) 
		{
			cout << nodes[i].seqid << " " << nodes[i].adj_list[j] << endl;
		}
	} 

  	return 0;
}
