#include <stdio.h>
#include <string>
#include <map>
#include <iostream>
#include <vector>

using namespace std;


int main( int argc, char *argv[] ) {
 
  string str1 = "GATATATGCATATACTT";
  string str2 = "ATAT";
  short mlen = str2.length(); /*Length of the motif*/
  vector<int> coords;
  /*Main loop*/
  for( int i=0; i<str1.length(); i++ ) 
  {
    if( str2 == str1.substr(i,mlen ) ) 
    {
        coords.push_back(i+1);     
    }
  }
  
  /*printing results*/
  for(int i=0; i<coords.size(); i++) {
    printf("%d ", coords[i]);
  }
  
  printf("\n");
  return 0;
}
