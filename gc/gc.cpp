/*
    
    
    Identifying Unknown DNA Quicklyclick to collapse

    A quick method used by early computer software to determine the language of a given piece of text was to analyze the frequency with which each letter appeared in the text. This strategy was used because each language tends to exhibit its own letter frequencies, and as long as the text under consideration is long enough, software will correctly recognize the language quickly and with a very low error rate.

    A similar scenario arises in molecular biology when researchers encounter a strand of DNA from an unknown species. Here, one possible method used to determine the species is to calculate the strand's GC-content, or the percentage of its bases that are either cytosine or guanine, for comparison against a genome database. For example, the GC-content of the DNA string AGCTATAG is 37.5%, as 3 of the 8 symbols are C or G. (Note that the reverse complement of a DNA string has the same GC-content, so that we only need to consider one strand of a DNA molecule.)

    In practice, the GC-content of most eukaryotic genomes hovers around 50%. However, because genomes are so long, we may be able to differentiate species based off very small discrepancies in GC-content; specifically, most prokaryotes have a GC-content significantly higher than 50%, so that GC-content can be used to quickly differentiate prokaryotes and eukaryotes given small samples of DNA.

	Problem

DNA strings must be labeled when they are consolidated into a database. A commonly used method of string labeling is called FASTA format. In this format, the string is introduced by a line that begins with ">", followed by some information naming and characterizing the string. Subsequent lines contain the string itself; the next line starting with ">" indicates the label of the next string.

In Rosalind's implementation, a string in FASTA format will be labeled by the ID "Rosalind_Sequence_xxxx", where "xxxx" denotes a four-digit code between 0000 and 9999.

Given: At most 10 DNA strings in FASTA format (of length at most 1 kbp each).

Return: The ID of the string having the highest GC-content, followed by the GC-content of that string, rounded to 2 decimal places.
Sample Dataset

>Rosalind_Sequence_6404
CCTGCGGAAGATCGGCACTAGAATAGCCAGAACCGTTTCTCTGAGGCTTCCGGCCTTCCC
TCCCACTAATAATTCTGAGG
>Rosalind_Sequence_5959
CCATCGGTAGCGCATCCTTAGTCCAATTAAGTCCCTATCCAGGCGCTCCGCCGAAGGTCT
ATATCCATTTGTCAGCAGACACGC
>Rosalind_Sequence_0808
CCACCCTCGTGGTATGGCTAGGCATTCAGGAACCGGAGAACGCTTCAGACCAGCCCGGAC
TGGGAACCTGCGGGCAGTAGGTGGAAT

Sample Output

Rosalind_Sequence_0808
60.92%



 */

#include <stdio.h>
#include <string>
#include <map>
#include <iostream> /*нужен для cout*/
#include <fstream> /*нужен для ifstream*/
#include <vector>

using namespace std;

int main( int argc, char *argv[] ) {
 	/*Loading results from file*/
	std:fstream infile;
	infile.open(argv[1]); 
	string line;
	string seqid;
	string seq = "";
	int line_cnt = 0;
	double cntgc;
	double linelen = 0.00;
	float gc_content = 0.000;
	float max = 0.000;
	string seqid_max;
	while( getline(infile, line) ) 
	{
	  if(line.substr(0,1) == ">") 
	  {
	    if(line_cnt != 0) 
	    {
	      /*parsing give genomic string*/
	      cntgc = 0.00;
	      linelen = seq.length();
	      gc_content = 0.00;
	      for(int i=0; i<seq.length(); i++) 
	      {
		if( (seq[i] == 'C') || (seq[i] == 'G') ) cntgc++;
	      }

	      gc_content = (cntgc/linelen)*100;
	      if( max < gc_content ) 
	      {
		max = gc_content;
		seqid_max = seqid;
	      }
	      cntgc = 0.00;
	    }
	    seqid = line.substr(1,line.length() - 1);
	    seq.clear();
	    continue;		
	  }

	  line_cnt++;
	  seq+= line ;
	}
	
	infile.close();
	/*Processing the last line*/
	/*parsing give genomic string*/

	cntgc = 0.00;
	linelen = seq.length();
	for(int i=0; i<linelen; i++) 
	{
	  if((seq[i] == 'C') || (seq[i] == 'G') ) cntgc++;
	}

	gc_content = (cntgc/linelen)*100;
	
	if( max < gc_content ) 
	{
	  max = gc_content;
	  seqid_max = seqid;
	}
	
	/*making output*/
	cout << seqid_max << endl;
	printf("%3.6f",max); cout << "%" << endl;
	
  	return 0;
}
