
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) 
{
  FILE * fp;
  long fSize;
  char *input_str = NULL;
  size_t result;//Just to control the return value of fread() function
  int a_cnt, c_cnt, g_cnt, t_cnt;
  
  fp = fopen ( argv[1] , "rb" );
  if (fp==NULL) {fputs ("File error",stderr); exit (1);}

  //Obtain file size:
  fseek (fp , 0 , SEEK_END);
  fSize = ftell (fp);
  rewind (fp);//return the pointer back to the beginning of the file

  //Allocate memory to contain the whole file:
  input_str = (char*) malloc (sizeof(char)*fSize);
  if (input_str == NULL) {fputs ("Memory error",stderr); exit (2);}

  /*Loading results from file*/
  result = fread (input_str,1,fSize,fp);
  if (result != fSize) {fputs ("Reading error",stderr); exit (3);}

  a_cnt = c_cnt = g_cnt = t_cnt = 0;
  
  /*Main loop*/
  int i = 0;
  while(i<fSize) {
    if(input_str[i] == 'A') a_cnt++;
    if(input_str[i] == 'C') c_cnt++;
    if(input_str[i] == 'G') g_cnt++;
    if(input_str[i] == 'T') t_cnt++;
    i++;
  }
  
  /*Printing results*/
  printf("%d %d %d %d\n",a_cnt, c_cnt, g_cnt, t_cnt);
  
  return 0;
  
}
