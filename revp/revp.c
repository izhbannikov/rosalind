#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <map>
#include <iostream> /*нужен для cout*/
#include <fstream> /*нужен для ifstream*/
#include <list>
#include <streambuf>
#include <vector>
#include <deque>
#include <algorithm>

using namespace std;

string MakeSeqComplement(string init_str) {
    
    for(int i = 0; i<init_str.length(); i++) {
        if(init_str[i] == 'A') {
            init_str[i] = 'T';
            continue;
        }
        if(init_str[i] == 'T') {
            init_str[i] = 'A';
            continue;
        }
        if(init_str[i] == 'G') {
            init_str[i] = 'C';
            continue;
        }
        if(init_str[i] == 'C') {
            init_str[i] = 'G';
            continue;
        }
        
    }
    
    return init_str;
}


int main( int argc, char *argv[] ) {
	string dnastr = "";
	
	fstream in(argv[1]);
	string line;
	while(getline(in, line)) {
	  if(line[0] == '>') continue;
	  dnastr += line;
	}
	
	short k;

	for( k=4; k<=12; k++ ) {
		for( int i=0; i<= dnastr.length() - k; i++ ) {
			string kmer = dnastr.substr(i,k);
			string rev_kmer = kmer;
			reverse( rev_kmer.begin(), rev_kmer.end() );
			rev_kmer = MakeSeqComplement(rev_kmer);
			
			if( kmer == rev_kmer ) cout << i+1 << " " << k << endl;
		}
	}
	
  	return 0;
}
