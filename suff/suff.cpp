#include <stdio.h>
#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>

using namespace std;

fstream out;
string S = "ATAAATG$";

class Node {
    public :
      unsigned int cnt; //Number of sons of the tree root
      vector<Node*> d; 
      unsigned int first; //number of the first symbol of substring - an edge label
      unsigned int last; //number of the last symbol of substring - an edge label
      Node *next; //Pointer to the son
      unsigned int num; //A label of leaf. It is 0 for internal edge
};

void traverse(Node *w) {
  //if(w == NULL) return;
  for(unsigned int j=0; j<w->cnt; j++) {
      for(unsigned int z = w->d[j]->first; z <= w->d[j]->last; z++) out << S[z];
      out << endl;
      if(w->d[j]->next != NULL) traverse(w->d[j]->next);
  }
  
  
}

int main(int argc, char **argv) {
  
  Node *w;
  Node *v;
  unsigned int n = S.length();
  unsigned int i, j, l, t;
  bool bl;
  i = j = l = t = 0;
  //First step: create the description of an edge that goes to the vertex with label of 1:
  unsigned int st = 1; //Leaf number
  
  Node *tree = new Node();
  tree->cnt = 1;
  tree->d.push_back(new Node());
  tree->d[0]->first = 0;
  tree->d[0]->last = n-1;
  tree->d[0]->next = NULL;
  tree->d[0]->num = st;
  //Main loop: we represent a suffix S[i..n] in a tree. 
  for(i = 1; i < n; ++i) {
    w = tree;
    t = i;
    bl = false;
    //Do this until the leaf is formed:
    while(!bl) {
      j = 0;
      bool ll = false;
      while( (ll == false) && (j < w->cnt) ) {
	if(S[w->d[j]->first] == S[t]) { //Found the edge - the first symbol of suffix is equal to the first symbol of edge:
	  l = j; ll = true;
	}
	j = j + 1;
      }
      if(!ll) { //The edge was not found - we then create a new leaf node whose father is w:
	  w->cnt = w->cnt + 1;
	  st = st + 1;
	  bl = true; //Suffix is processed
	  w->d.push_back(new Node());
	  w->d[w->cnt-1]->next = NULL;
	  w->d[w->cnt-1]->num = st;
	  w->d[w->cnt-1]->first = t;
	  w->d[w->cnt-1]->last = n-1;
      } else { //The edge was not found - compare suffix symbols and the edge labels
      	j = 0;
	
	while( (w->d[l]->first+j <= w->d[l]->last) && (S[t+j] == S[w->d[l]->first + j]) ) {
	  j = j + 1;
	}
	
	if(w->d[l]->first + j <= w->d[l]->last) {
	    //Create a new internal node
	    st = st + 1;
	    v = new Node();
	    v->cnt = 2;//This node has 2 sons
	    v->d.push_back(new Node());
	    v->d.push_back(new Node());
	    v->d[0]->next = w->d[l]->next;
	    //A new edge's label is equal to the part of the suffix and the old edge label
	    v->d[0]->num = w->d[l]->num;
	    v->d[0]->first = w->d[l]->first + j;
	    v->d[0]->last = w->d[l]->last;
	    v->d[1]->next = NULL;
	    //The rest of the suffix is an edge label, which is going to the leaf with the number of suffix under procession.
	    v->d[1]->num = st;
	    v->d[1]->first = t + j;
	    v->d[1]->last = n-1;
	    //Correcting the description of the father's node
	    w->d[l]->next = v;
	    w->d[l]->last = w->d[l]->first + j- 1;
	    w->d[l]->num = 0;
	    bl = true;//Suffix is processed
	} else { //Go to the next node of the tree - the part of suffix matched to the edge's label
	    t = t + w->d[l]->last - w->d[l]->first + 1;
	    w = w->d[l]->next;
	}
	
      }//End of the processing of found label
      
    }//End of the cycle of suffix processing
  }

  out.open("out.txt",ios::out);
  //Traverse tree:
  w=tree;
  traverse(w);
  
  
} //End main